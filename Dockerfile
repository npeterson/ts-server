FROM node:alpine
WORKDIR /app

COPY ./package.json ./

RUN npm install
COPY . .

# runs start command in docker-compose 
CMD ["npm", "run", "tsc"]