const path = require('path');
module.exports = {
    development: {
        client: 'pg',
        connection: {
            host: process.env.PGHOST,
            port: process.env.PGPORT,
            user: process.env.PGUSER,
            password: process.env.PGPASSWORD,
            database: process.env.PGDATABASE
        },
        migrations: {
            directory: 'dist/db/migrations'
        },
        seeds: {
            directory: 'dist/db/seeds'
        }
    },
    production: {
        client: 'pg',
    }
};
//# sourceMappingURL=knexfile.js.map