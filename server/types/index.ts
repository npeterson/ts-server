export interface UserData {
  firstName: string;
  lastName: string;
  organizationId? : string;
  address: object;
}
export interface OrganizationData {
  name: string;
  id: string;
}