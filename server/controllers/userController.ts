import { Request, Response } from 'express'
import User from '../models/User'

export let createUser = async (req: Request, res: Response) => {
  try {
    const { firstName, lastName, organizationId, address } = req.body
    let response = await User
      .query()
      .insert({
        first_name : firstName,
        last_name : lastName,
        organization_id: organizationId,
        address
      })
    return res.status(200).json(response)
  } catch (e) {
    return res.status(400).json(e)
  }
}

export const getUsers = async (req: Request, res: Response) => {
  try {
    let response = await User.query()
    return res.status(200).json(response)
  } catch (e) {
    return res.status(400).json({message: e})
  }
}

export const getUser = async (req: Request, res: Response) => {
  try {
    let response = await User.query().where({id: req.params.id})
    return res.status(200).json(response)
  } catch (e) {
    return res.status(400).json({message: e})
  }
}

export const patchUser = async (req: Request, res: Response) => {
  try {
    const { firstName, lastName, address, organizationId } = req.body
    const { id } = req.params
    let response = await User
      .query()
      .patch({
        first_name : firstName,
        last_name : lastName, 
        organization_id : organizationId, 
        address
      })
      .where({ id })
      .returning('*')
      .first()
    return res.status(200).json(response)
  } catch (e) {
    return res.status(400).json({message: e})
  }
}

export const deleteUser = async (req: Request, res: Response) => {
  try {
    const { id } = req.params
    let response = await User
      .query()
      .delete()
      .where({ id })
    return res.status(200).json(response)
  } catch (e) {
    return res.status(400).json({message: e})
  }
}