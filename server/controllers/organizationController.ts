import { Request, Response } from 'express'
import { transaction } from 'objection'
import Organization from '../models/Organization'
import User from '../models/User'
import { formatUser } from '../utilities'

export const createOrganization = async (req: Request, res: Response) => {
  try {
    const { name } : { name: string } = req.body
    let response = await Organization
      .query()
      .insert({ name })
    return res.status(200).json(response)
  } catch (e) {
    return res.status(400).json(e)
  }
}

export const getOrganizations = async (req: Request, res: Response) => {
  try {
    let response = await Organization.query()
    return res.status(200).json(response)
  } catch (e) {
    return res.status(400).json({message: e})
  }
}

export const getOrganization = async (req: Request, res: Response) => {
  try {
    let org = await Organization
      .query()
      .where({id: req.params.id})
    let users = await User
      .query()
      .where({organization_id: req.params.id})
    return res.status(200).json({ ...org, users })
  } catch (e) {
    return res.status(400).json({message: e})
  }
}

export const patchOrganization = async (req: Request, res: Response) => {
  try {
    const { name } = req.body
    const { id } = req.params
    let response = await Organization
      .query()
      .patch({ name })
      .where({ id })
      .returning('*')
      .first()
    return res.status(200).json(response)
  } catch (e) {
    return res.status(400).json({message: e})
  }
}

export const deleteOrganization = async (req: Request, res: Response) => {
  try {
    const { id } = req.params
    let response = await Organization
      .query()
      .delete()
      .where({ id })
    return res.status(200).json(response)
  } catch (e) {
    return res.status(400).json({message: e})
  }
}

export const createGroup = async (req: Request, res: Response) => {
  // insert both an organization and a group of users
  const { organization, user } = req.body
  try {
    const bulkInsert = await transaction(Organization, User, async (Organization, User) => {
      const orgResult = await Organization
        .query()
        .insert(organization)
        .returning('*')
        .first()
      if(!orgResult) {
        throw new Error('There was a problem inserting the new organization')
      }
      let formattedUser = formatUser(user, orgResult)
      return User
          .query()
          .insert(formattedUser)
      })
    res.status(200).json(bulkInsert)
  } catch(e) {
    const errorMessage = e.message || 'Error, neither organization nor group were inserted.'
    res.status(400).json({message: e || errorMessage})
  }
}