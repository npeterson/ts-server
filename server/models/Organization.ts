import { Model, RelationMappings } from 'objection'
import User from './User'

export default class Organization extends Model {
  readonly id: string;
  name: string;
  

  static get tableName() {
    return 'organizations'
  }

  static get idColumn() {
    return 'id'
  }

  static get jsonSchema () {
    return {
      type: 'object',
      required: ['name'],
      properties: {
        id: {type: 'integer'},
        name: {type: 'string', minLength: 1, maxLength: 255},
        address: { type: 'object',
          properties: {
            street: {type: 'string'},
            city: {type: 'string'},
            zipCode: {type: 'string'}
        }
        }
      }
    }
  }
  static get relationMappings() {
    return {
      users: {
        relation: Model.HasManyRelation,
        modelClass: `${__dirname}/User`,
        join: {
          from: 'organizations.id',
          to: 'users.organization_id'
        }
      }
    }
  }
}