import knex from '../db/knex'
import { Model } from 'objection'
import Organization from './Organization'

export default class User extends Model {
  readonly id!: string;
  'first_name': string;
  'last_name': string;
  address?: object;
  'organization_id': string;

  static get tableName() {
    return 'users'
  }

  static get idColumn() {
    return 'id'
  }

  static get jsonSchema () {
    return {
      type: 'object',
      required: ['first_name', 'last_name'],
      properties: {
        id: {type: 'integer'},
        firstName: {type: 'string', minLength: 1, maxLength: 255},
        lastName: {type: 'string', minLength: 1, maxLength: 255},
        address: {
          type: 'object',
          properties: {
            street: {type: 'string'},
            city: {type: 'string'},
            zipCode: {type: 'string'}
          }
        },
        organizationId: { type: 'string' }
      }
    }
  }

  static relationMappings = {
    organization: {
      relation: Model.BelongsToOneRelation,
      modelClass: Organization,
      join: {
        from: 'organizations.userId',
        to: 'users.id'
      }
    }
  }
  // static createUser = async (firstName: string, lastName: string) => {
  //   try {
  //     let response = await knex('users')
  //     .insert({ first_name: firstName, last_name: lastName })
  //     .returning('*')
  //     // database returns an array with one item
  //     let [element] = response
  //     if(!element) throw new Error('Could not save record')
  //     return { status: 200, message: element }
  //   } catch (e) {
  //     return { status: 400, message: e } 
  //   }
  // }

  static verifyUser = async (id : any, trx: any) => {
    if(id === 5) throw new Error('I do not like five!')
  }

  static createUsers = async (users: []) => {
    try {
      return knex.transaction((trx: any) => {
        knex('users')
          .transacting(trx)
          .insert({name : 'fdslkfdssdfa'})
          .then((response : any) => {
            let [id] = response
            return User.verifyUser(id, trx)
          })
          .then(trx.commit)
          .catch(trx.rollback)
      })
    } catch (e) {
      return { status: 400, message: e }
    }
  }
  // Hooks for queries
  // $beforeInsert() {
  //   this.createdAt = new Date();
  //   this.updatedAt = new Date();
  // }

  // $beforeUpdate() {
  //   this.updatedAt = new Date();
  // }
}