const path = require('path')

module.exports = {

  development: {
    client: 'pg',
    // change to docker connection
    connection: {
      host: process.env.PGHOST,
      port: process.env.PGPORT,
      user: process.env.PGUSER,
      password: process.env.PGPASSWORD,
      database: process.env.PGDATABASE
    },
    migrations: {
      directory: path.join(__dirname, 'db', 'migrations')
    },
    seeds: {
      directory: path.join(__dirname, 'db', 'seeds')
    }
  },
  production: {
    client: 'pg',
    // connect to db through environment variable
    // connection: DATABASE_CONNECTION
  }

};
