import { UserData, OrganizationData } from '../types'

export const formatUser = (userData: UserData, organizationData: OrganizationData) => {
  const { firstName, lastName, address } = userData
  if (!firstName || !lastName) {
    throw new Error('Users must have a first and a last name')
  }
  return {
    first_name: userData.firstName,
    last_name: userData.lastName,
    address,
    organization_id: organizationData.id
  }
}