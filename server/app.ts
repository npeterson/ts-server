import express = require('express')
const app = express()
const PORT = process.env.PORT || 5555
const listener = () => console.log(`Listening on port ${PORT}`)

import bodyParser from 'body-parser'
import cors from 'cors'
import morgan from 'morgan'
app.use(bodyParser.json())
  .use(cors())
  .use(morgan('dev'))

app.disable('x-powered-by')
import { 
  createUser,
  getUsers,
  getUser,
  patchUser,
  deleteUser,
  createOrganization,
  getOrganization,
  getOrganizations,
  patchOrganization,
  deleteOrganization,
  createGroup
} from "./controllers"

app.get('/users/:id', getUser)
app.get('/users', getUsers)
app.post('/users', createUser)
app.patch('/users/:id', patchUser)
app.delete('/users/:id', deleteUser)

app.get('/organizations/:id', getOrganization)
app.get('/organizations', getOrganizations)
app.post('/organizations', createOrganization)
app.patch('/organizations/:id', patchOrganization)
app.delete('/organizations/:id', deleteOrganization)

app.post('/group', createGroup)

app.get('/', (_: express.Request, res : express.Response) => {
  res.json({ message: 'Typescript server root route'})
})

app.listen(PORT, listener)