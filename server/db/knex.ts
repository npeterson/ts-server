
const environment = process.env.NODE_ENV || 'development'
const knexConfig = require('../knexfile')[environment]
const { Model } = require('objection')
const knexConnection = require('knex')(knexConfig)


export default Model.knex(knexConnection)