
exports.up = function(knex: any) {
  return knex.schema.createTable('users', (table : any) => {
    table.increments('id').primary()
    table.string('first_name').notNullable().defaultsTo('')
    table.string('last_name').notNullable().defaultsTo('')
    table.json('address')
    table.integer('organization_id')
      .references('id')
      .inTable('organizations')
      .index()
    table.foreign('')
    table.timestamps()
  })
}

exports.down = function(knex: any) {
  return knex.schema.dropTable('users')
}
