exports.up = function(knex: any) {
  return knex.schema.createTable('organizations', (table : any) => {
    table.increments('id').primary()
    table.string('name').notNullable().defaultsTo('')
    table.json('address')
    table.timestamps()
  })
}

exports.down = function(knex: any) {
  return knex.schema.dropTable('organizations')
}
