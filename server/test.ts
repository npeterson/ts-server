const axios = require('axios')
const { expect, assert } = require('chai')
const { describe, it } = require('mocha')
const API = process.env.API || `http://localhost:5555`

const user1 = {
  firstName: 'Captain',
  lastName: 'Barnacles'
}

const user2 = {
  firstName: 'Emma',
  lastName: 'Racoon'
}

const user3 = {
  firstName: 'Mister',
  lastName: 'Rogers'
}

const org1 = {
  name: 'test organization'
}
interface InvalidUser {
  firstName?: any;
  lastName?: any;
}

const invalidUser : InvalidUser = {
  firstName: 432532543,
  lastName: null
}

describe('add, update, and delete user', () => {
  it('adds and deletes a user', async () => {
    const { data } = await axios.post(`${API}/users`, user1)
    expect(data.first_name).to.equal(user1.firstName)
    expect(data.last_name).to.equal(user1.lastName)
    const cleanupRecord = await axios.delete(`${API}/users/${data.id}`)
    expect(cleanupRecord.data).to.equal(1)
  })
  it('adds and updates a user', async () => {
    const expectedNewName : string = 'Larry'
    const { data } = await axios.post(`${API}/users`, user2)
    expect(data.first_name).to.equal(user2.firstName)
    expect(data.last_name).to.equal(user2.lastName)
    const updatedData = await axios.patch(`${API}/users/${data.id}`, {firstName : expectedNewName })
    expect(updatedData.data.first_name).to.equal(expectedNewName)
    const cleanupRecord = await axios.delete(`${API}/users/${data.id}`)
    await expect(cleanupRecord.data).to.equal(1)
  })
})

describe('handles transactions', () => {
  it('correctly inserts multiple records with transactions', async () => {
    const { data } = await axios.post(`${API}/group`, { organization: org1, user: user3 })
    expect(data.first_name).to.equal(user3.firstName)
    expect(data.last_name).to.equal(user3.lastName)
    const cleanupRecord = await axios.delete(`${API}/users/${data.id}`)
    await expect(cleanupRecord.data).to.equal(1)
  })

  it('rolls back transactions when invalid values are passed', async () => {
    try {
      await axios.post(`${API}/group`, {organization: org1, user: invalidUser})
    } catch (e) {
      expect(e.response.status).to.equal(400)
      expect(e.response.statusText).to.equal('Bad Request')
    }
  })
  // this is a hack :(
  it('exits', () => {
    process.exit(0)
  })
})


